# Contributor: Sheila Aman <sheila@vulpine.house>
# Maintainer: Sheila Aman <sheila@vulpine.house>
# keep these in sync with kernel version
_kflavour=""
_kver="5.4.5-mc0"
_kver2="$_kver-easy"
pkgname="zfs-$_kver"
pkgver=0.8.3
pkgrel=0
pkgdesc="Linux support for the ZFS filesystem"
url="https://open-zfs.org"
arch="all"
license="CDDL"
depends="easy-kernel$_kflavour-modules-$_kver zfs-utils"
makedepends="eudev-dev libtirpc-dev linux-headers openssl-dev zlib-dev
	easy-kernel$_kflavour-src-$_kver autoconf automake libtool"
install=""
subpackages="$pkgname-dev"
source="https://github.com/zfsonlinux/zfs/releases/download/zfs-$pkgver/zfs-$pkgver.tar.gz
	fix-autoconf-redirection-for-dash.patch
	Linux-5.6-compat-time_t-2c3a83701dd1.patch
	Linux-5.6-compat-timestamp_truncate-795699a6cc28.patch
	Linux-5.6-compat-ktime_get_raw_ts64-ff5587d65137.patch"
builddir="$srcdir/zfs-$pkgver"

prepare() {
	default_prepare
	update_config_sub
	./autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-linux=/usr/src/linux-$_kver \
		--with-spec=generic \
		--with-config=kernel
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	mv "$pkgdir/usr/src/spl-$pkgver" "$subpkgdir/usr/src"
	cd "$pkgdir/usr/src/zfs-$pkgver"
	mv zfs.release.in zfs_config.h.in "$subpkgdir/usr/src/zfs-$pkgver"
	cd "$pkgdir/usr/src/zfs-$pkgver/$_kver2"
	mv Module.symvers zfs.release spl.release "$subpkgdir/usr/src/zfs-$pkgver/$_kver2"
	rm -r "$pkgdir/usr/src"
}

sha512sums="aded632e6b04180560d4f2ff283260016e883dadae4e7fda9070b7113dee948099cb7a7b183f1c8139654389a2610fb9cc6f997acdc846040e605125cf016010  zfs-0.8.3.tar.gz
47dc563fc1daa4c67096d2316ed2cea6aeaf4ca9e0daa41f2036a4ff3dcb542c88534ac25033a23e8fa97a9b82e56a8933f9fa56bae8a1fa07c1b15eb18f68f8  fix-autoconf-redirection-for-dash.patch
a74bb30ef2e5f28fdc7b0cfe915f2841762ec8f6cd2368d2e4ba8627443e8e0754011a88b743e27519d191464e35e6c923510451f2a86d7e1a4ad3c5b737015f  Linux-5.6-compat-time_t-2c3a83701dd1.patch
a791406265a6e1501e743b6f13764736439a14a9dfc1feaabc5fc82e27de1b2adf56b566c162fc42313f311fbf76c3213f7c2c05a518de36f96887e3d462e94a  Linux-5.6-compat-timestamp_truncate-795699a6cc28.patch
9c78793a5840bc5fb5bb44a210124c1c8f09ce0ddfe88b91be8dbde5465763912308b89cd87300fd05b898076366528612c34a9c2f834d3b6573aa68eb442ec3  Linux-5.6-compat-ktime_get_raw_ts64-ff5587d65137.patch"
