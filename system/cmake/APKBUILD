# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=cmake
pkgver=3.23.1
pkgrel=0
pkgdesc="Cross-platform build system"
url="https://cmake.org"
arch="all"
license="CMake"
depends=""
checkdepends="musl-utils file"
makedepends="ncurses-dev curl-dev expat-dev zlib-dev bzip2-dev libarchive-dev
	libuv-dev xz-dev rhash-dev"
subpackages="$pkgname-doc"

case $pkgver in
*.*.*.*) _v=v${pkgver%.*.*};;
*.*.*) _v=v${pkgver%.*};;
esac

source="https://cmake.org/files/$_v/cmake-${pkgver}.tar.gz"

_parallel_opt() {
	local i n
	for i in $MAKEFLAGS; do
		case "$i" in
			-j*) n=${i#-j};;
		esac;
	done
	[ -n "$n" ] && echo "--parallel=$n"
}

build() {
	# jsoncpp needs cmake to build so to avoid recursive build
	# dependency, we use the bundled version of jsoncpp
	./bootstrap \
		--prefix=/usr \
		--mandir=/share/man \
		--datadir=/share/$pkgname \
		--docdir=/share/doc/$pkgname \
		--system-libs \
		--no-system-jsoncpp \
		$(_parallel_opt)
	make
}

check() {
	# disable only one part of this test (see #417)
	sed -i Tests/RunCMake/ctest_submit/RunCMakeTest.cmake \
		-e '/^run_ctest_submit_FailDrop(https)/d' \
		;

	# skip CTestTestUpload: tries to upload something during check...
	#CTEST_PARALLEL_LEVEL=${JOBS} \
	CTEST_OUTPUT_ON_FAILURE=TRUE \
	bin/ctest \
		-E CTestTestUpload \
		;
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="94893f888c0cbfc58e54a0bd65d6c0697fe4a0e95c678b7cb35e7dc8854d57eb360bfc952750f97983348817f847f6df85903f21a5857b1a3880b2a7eb6cc029  cmake-3.23.1.tar.gz"
