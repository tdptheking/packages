# Maintainer: Adelie Platform Group <adelie-devel@lists.adelielinux.org>
pkgname=gcc
pkgver=8.5.0
[ "$BOOTSTRAP" = "nolibc" ] && pkgname="gcc-pass2"
[ "$CBUILD" != "$CHOST" ] && _cross="-$CARCH" || _cross=""
[ "$CHOST" != "$CTARGET" ] && _target="-$CTARGET_ARCH" || _target=""

pkgname="$pkgname$_target"
pkgrel=1
pkgdesc="The GNU Compiler Collection"
url="https://gcc.gnu.org"
arch="all"
license="GPL-3.0+ AND LGPL-2.1+"
_gccrel=$pkgver-r$pkgrel
depends="binutils$_target isl"
makedepends_build="gcc$_cross g++$_cross byacc flex texinfo zip gmp-dev
	mpfr-dev mpc1-dev zlib-dev"
makedepends_host="linux-headers gmp-dev mpfr-dev mpc1-dev isl-dev zlib-dev
	libucontext-dev"
subpackages=" "
[ "$CHOST" = "$CTARGET" ] && subpackages="gcc-doc$_target gcc-lang$_target"
replaces="libstdc++ binutils"
options="$options !check !dbg strip toolchain"

: ${LANG_CXX:=true}
: ${LANG_OBJC:=true}
: ${LANG_GO:=true}
: ${LANG_FORTRAN:=true}
: ${LANG_ADA:=true}

LIBGOMP=true
LIBGCC=true
LIBATOMIC=true
LIBITM=true

if [ "$CHOST" != "$CTARGET" ]; then
	if [ "$BOOTSTRAP" = nolibc ]; then
		LANG_CXX=false
		LANG_ADA=false
		LIBGCC=false
		_builddir="$srcdir/build-cross-pass2"
	else
		_builddir="$srcdir/build-cross-final"
	fi
	LANG_OBJC=false
	LANG_GO=false
	LANG_FORTRAN=false
	LIBGOMP=false
	LIBATOMIC=false
	LIBITM=false

	# reset target flags (should be set in crosscreate abuild)
	# fixup flags. seems gcc treats CPPFLAGS as global without
	# _FOR_xxx variants. wrap it in CFLAGS and CXXFLAGS.
	export CFLAGS="$CPPFLAGS $CFLAGS"
	export CXXFLAGS="$CPPFLAGS $CXXFLAGS"
	unset CPPFLAGS
	export CFLAGS_FOR_TARGET=" "
	export CXXFLAGS_FOR_TARGET=" "
	export LDFLAGS_FOR_TARGET=" "

	STRIP_FOR_TARGET="$CTARGET-strip"
elif [ "$CBUILD" != "$CHOST" ]; then
	# fixup flags. seems gcc treats CPPFLAGS as global without
	# _FOR_xxx variants. wrap it in CFLAGS and CXXFLAGS.
	export CFLAGS="$CPPFLAGS $CFLAGS"
	export CXXFLAGS="$CPPFLAGS $CXXFLAGS"
	unset CPPFLAGS

	# reset flags and cc for build
	export CC_FOR_BUILD="gcc"
	export CXX_FOR_BUILD="g++"
	export CFLAGS_FOR_BUILD=" "
	export CXXFLAGS_FOR_BUILD=" "
	export LDFLAGS_FOR_BUILD=" "
	export CFLAGS_FOR_TARGET=" "
	export CXXFLAGS_FOR_TARGET=" "
	export LDFLAGS_FOR_TARGET=" "

	# Languages that do not need bootstrapping
	LANG_OBJC=false
	LANG_GO=false
	LANG_FORTRAN=false

	STRIP_FOR_TARGET=${CROSS_COMPILE}strip
	_builddir="$srcdir/build-cross-native"
else
	STRIP_FOR_TARGET=${CROSS_COMPILE}strip
	_builddir="$srcdir/build"
fi

# libitm has TEXTRELs in ARM build, so disable for now
# gcc itself has TEXTRELs in m68k; until this is fixed, we need it
case "$CTARGET_ARCH" in
arm*)		LIBITM=false ;;
m68k)		options="$options textrels" ;;
ppc)		options="$options textrels" ;;
esac

# Fortran uses libquadmath if toolchain has __float128
# currently on x86, x86_64 and ia64
LIBQUADMATH=$LANG_FORTRAN
case "$CTARGET_ARCH" in
pmmx | x86 | x86_64)	LIBQUADMATH=$LANG_FORTRAN ;;
*)			LIBQUADMATH=false ;;
esac

# libatomic is a dependency for openvswitch
$LIBATOMIC && subpackages="$subpackages libatomic::$CTARGET_ARCH"
$LIBGCC && subpackages="$subpackages libgcc::$CTARGET_ARCH"
$LIBQUADMATH && subpackages="$subpackages libquadmath::$CTARGET_ARCH"
if $LIBGOMP; then
	depends="$depends libgomp=$_gccrel"
	subpackages="$subpackages libgomp::$CTARGET_ARCH"
fi

_languages=c
if $LANG_CXX; then
	subpackages="$subpackages libstdc++:libcxx:$CTARGET_ARCH g++$_target:gpp"
	_languages="$_languages,c++"
fi
if $LANG_OBJC; then
	subpackages="$subpackages libobjc::$CTARGET_ARCH gcc-objc$_target:objc"
	_languages="$_languages,objc"
fi
if $LANG_GO; then
	subpackages="$subpackages libgo::$CTARGET_ARCH gcc-go$_target:go"
	_languages="$_languages,go"
fi
if $LANG_FORTRAN; then
	subpackages="$subpackages libgfortran::$CTARGET_ARCH gfortran$_target:gfortran"
	_languages="$_languages,fortran"
fi
if $LANG_ADA; then
	subpackages="$subpackages libgnat::$CTARGET_ARCH gcc-gnat$_target:gnat"
	_languages="$_languages,ada"
	makedepends_build="$makedepends_build gcc-gnat gcc-gnat$_cross"
fi
makedepends="$makedepends_build $makedepends_host"

source="https://ftp.gnu.org/gnu/gcc/gcc-$pkgver/gcc-$pkgver.tar.xz

	0002-posix_memalign.patch
	0008-s390x-muslldso.patch
	0009-microblaze-pr65649.patch
	0010-ldbl128-config.patch
	0011-m68k.patch
	0012-static-pie.patch
	0016-invalid-tls-model.patch

	002_all_default-relro.patch
	005_all_default-as-needed.patch
	006_as-needed-gold.patch
	011_all_default-warn-format-security.patch
	012_all_default-warn-trampolines.patch
	020_all_msgfmt-libstdc++-link.patch
	051_all_libiberty-pic.patch

	201-ada.patch
	202-ibm-ldbl.patch
	205-nopie.patch

	libgcc-always-build-gcceh.a.patch
	gcc-4.9-musl-fortify.patch
	gcc-6.1-musl-libssp.patch
	gcc-pure64.patch

	fix-cxxflags-passing.patch
	ada-shared.patch

	320-libffi-gnulinux.patch
	330-gccgo-link-to-ucontext.patch
	331-gccgo-use-real-off_t-type.patch
	332-gccgo-sysinfo.patch
	333-gccgo-signal-ppc64.patch
	334-gccgo-signal-shell.patch
	335-gccgo-signal-ppc32.patch
	336-gccgo-mmap64.patch
	337-gccgo-signal-sig34.patch
	338-gccgo-syscall.patch
	339-gccgo-errstr.patch
	340-gccgo-time64-stat.patch

	add-classic_table-support.patch
	gcc-5.4.0-locale.patch

	backport-r267157-posix-conformant-snprintf.patch

	libgo-musl-1.2.3.patch
	"

# we build out-of-tree
_gccdir="$srcdir"/gcc-$pkgver
_gcclibdir=/usr/lib/gcc/${CTARGET}/$pkgver
_gcclibexec=/usr/libexec/gcc/${CTARGET}/$pkgver

prepare() {
	cd "$_gccdir"

	_err=
	for i in $source; do
		case "$i" in
		*.patch)
			msg "Applying $i"
			patch -p1 -F3 -i "$srcdir"/$i || _err="$_err $i"
			;;
		esac
	done

	if [ -n "$_err" ]; then
		error "The following patches failed:"
		for i in $_err; do
			echo "  $i"
		done
		return 1
	fi

	echo ${pkgver} > gcc/BASE-VER
}

build() {
	local _arch_configure=
	local _libc_configure=
	local _cross_configure=
	local _bootstrap_configure=
	local _hash_style=gnu
	local _symvers=

	cd "$_gccdir"

	case "$CTARGET" in
	aarch64-*-*-*)		_arch_configure="--with-arch=armv8-a --with-abi=lp64 --enable-fix-cortex-a53-835769 --enable-fix-cortex-a53-843419";;
	armv5-*-*-*eabi)	_arch_configure="--with-arch=armv5te --with-tune=arm926ej-s --with-float=soft --with-abi=aapcs-linux";;
	armv6-*-*-*eabihf)	_arch_configure="--with-arch=armv6zk --with-tune=arm1176jzf-s --with-fpu=vfp --with-float=hard --with-abi=aapcs-linux";;
	armv7-*-*-*eabihf)	_arch_configure="--with-arch=armv7-a --with-tune=generic-armv7-a --with-fpu=vfpv3-d16 --with-float=hard --with-abi=aapcs-linux --with-mode=thumb";;
	m68k-*-*-*)		_arch_configure="--with-arch=68020 --with-tune=68020-40";;
	mipsel-*-*-*)		_arch_configure="--with-arch-32=mips2 --with-tune-32=mips32 --with-fp-32=32 --with-mips-plt --with-float=hard --with-abi=32";;
	mips-*-*-*)		_arch_configure="--with-arch=mips3 --with-mips-plt --with-abi=32"; _hash_style="sysv";;
	mips32el-*-*-*)		_arch_configure="--with-arch=mips32 --with-mips-plt --with-abi=32"; _hash_style="sysv";;
	powerpc-*-*-*)		_arch_configure="--enable-secureplt --enable-decimal-float=no";;
	powerpc64*-*-*-*)	_arch_configure="--with-abi=elfv2 --enable-secureplt --enable-decimal-float=no";;
	powerpc64le*-*-*-*)	_arch_configure="--with-abi=elfv2 --enable-secureplt --enable-decimal-float=no";;
	i486-*-*-*)		_arch_configure="--with-arch=i486 --with-tune=generic --enable-cld";;
	i586-*-*-*)		_arch_configure="--with-arch=i586 --with-tune=pentium2 --enable-cld --enable-mmx";;
	pentium3-*-*-*)		_arch_configure="--with-arch=pentium3 --with-tune=pentium-m";;
	s390x-*-*-*)		_arch_configure="--with-arch=z196 --with-tune=zEC12 --with-zarch --with-long-double-128 --enable-decimal-float";;
	esac

	case "$CTARGET_LIBC" in
	musl)
		# musl does not support mudflap
		# libmpx uses secure_getenv and struct _libc_fpstate not present in musl
		_libc_configure="--disable-libmpx --disable-libmudflap --disable-libsanitizer"
		_symvers="--disable-symvers"
		export libat_cv_have_ifunc=no
		export ac_cv_type_off64_t=no
		;;
	esac

	[ "$CBUILD" != "$CHOST"   ] && _cross_configure="--disable-bootstrap"
	[ "$CHOST"  != "$CTARGET" ] && _cross_configure="--disable-bootstrap --with-sysroot=$CBUILDROOT"

	case "$BOOTSTRAP" in
	nolibc)	_bootstrap_configure="--with-newlib --disable-shared --enable-threads=no" ;;
	*)	_bootstrap_configure="--enable-shared --enable-threads --enable-tls" ;;
	esac

	$LIBGOMP	|| _bootstrap_configure="$_bootstrap_configure --disable-libgomp"
	$LIBATOMIC	|| _bootstrap_configure="$_bootstrap_configure --disable-libatomic"
	$LIBITM		|| _bootstrap_configure="$_bootstrap_configure --disable-libitm"
	$LIBQUADMATH	|| _arch_configure="$_arch_configure --disable-libquadmath"

	msg "Building the following:"
	echo ""
	echo "  CBUILD=$CBUILD"
	echo "  CHOST=$CHOST"
	echo "  CTARGET=$CTARGET"
	echo "  CTARGET_ARCH=$CTARGET_ARCH"
	echo "  CTARGET_LIBC=$CTARGET_LIBC"
	echo "  languages=$_languages"
	echo "  arch_configure=$_arch_configure"
	echo "  libc_configure=$_libc_configure"
	echo "  cross_configure=$_cross_configure"
	echo "  bootstrap_configure=$_bootstrap_configure"
	echo "  hash_style=$_hash_style"
	echo ""

	export enable_libstdcxx_time=rt

	mkdir -p "$_builddir"
	cd "$_builddir"
	"$_gccdir"/configure --prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--build=${CBUILD} \
		--host=${CHOST} \
		--target=${CTARGET} \
		--with-pkgversion="Adelie ${pkgver}" \
		--with-bugurl="https://bts.adelielinux.org/" \
		--enable-checking=release \
		--disable-fixed-point \
		--disable-libstdcxx-pch \
		--disable-multiarch \
		--disable-multilib \
		--disable-werror \
		$_symvers \
		--enable-__cxa_atexit \
		--enable-default-pie \
		--enable-default-ssp \
		--enable-cloog-backend \
		--enable-languages=$_languages \
		$_arch_configure \
		$_libc_configure \
		$_cross_configure \
		$_bootstrap_configure \
		--with-system-zlib \
		--with-linker-hash-style=$_hash_style
	make
}

check() {
	cd "$_builddir"
	make check
}

package() {
	cd "$_builddir"
	make -j1 DESTDIR="${pkgdir}" install

	ln -s gcc "$pkgdir"/usr/bin/cc

	# strip debug info from some static libs
	${STRIP_FOR_TARGET} -g `find "$pkgdir" \( -name libgfortran.a -o -name libobjc.a -o -name libgomp.a \
		-o -name libmudflap.a -o -name libmudflapth.a \
		-o -name libgcc.a -o -name libgcov.a -o -name libquadmath.a \
		-o -name libitm.a -o -name libgo.a -o -name libcaf\*.a \
		-o -name libatomic.a -o -name libasan.a -o -name libtsan.a \) \
		-a -type f`

	if $LIBGOMP; then
		mv "$pkgdir"/usr/lib/libgomp.spec "$pkgdir"/$_gcclibdir
	fi
	if $LIBITM; then
		mv "$pkgdir"/usr/lib/libitm.spec "$pkgdir"/$_gcclibdir
	fi

	# remove ffi
	rm -f "$pkgdir"/usr/lib/libffi* "$pkgdir"/usr/share/man/man3/ffi*
	find "$pkgdir" -name 'ffi*.h' | xargs rm -f

	local gdblib="${_target:+$CTARGET/}lib"
	for i in $(find "$pkgdir"/usr/$gdblib/ -type f -maxdepth 1 -name "*-gdb.py" ); do
		mkdir -p "$pkgdir"/usr/share/gdb/python/auto-load/usr/$gdblib
		mv "$i" "$pkgdir"/usr/share/gdb/python/auto-load/usr/$gdblib/
	done

	# move ada runtime libs
	if $LANG_ADA; then
		for i in $(find "$pkgdir"/$_gcclibdir/adalib/ -type f -maxdepth 1 -name "libgna*.so"); do
			mv "$i" "$pkgdir"/usr/lib/
			ln -s ../../../../${i##*/} $i
		done
	fi

	if [ "$CHOST" != "$CTARGET" ]; then
		# cross-gcc: remove any files that would conflict with the
		# native gcc package
		rm -rf "$pkgdir"/usr/bin/cc "$pkgdir"/usr/include "$pkgdir"/usr/share
		# libcc1 does not depend on target, don't ship it
		rm -rf "$pkgdir"/usr/lib/libcc1.so*

		# fixup gcc library symlinks to be linker scripts so
		# linker finds the libs from relocated sysroot
		for so in "$pkgdir"/usr/$CTARGET/lib/*.so; do
			if [ -h "$so" ]; then
				local _real="$(basename $(readlink "$so"))"
				rm -f "$so"
				echo "GROUP ($_real)" > "$so"
			fi
		done
	fi
}

libatomic() {
	pkgdesc="GCC Atomic library"
	depends=
	replaces="gcc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libatomic.so.* "$subpkgdir"/usr/lib/
}

libcxx() {
	pkgdesc="GNU C++ standard runtime library"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libstdc++.so.* "$subpkgdir"/usr/lib/
}

gpp() {
	pkgdesc="GNU C++ standard library and compiler"
	depends="libstdc++=$_gccrel gcc=$_gccrel libc-dev"
	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/usr/bin \
		"$subpkgdir"/usr/${_target:+$CTARGET/}include \
		"$subpkgdir"/usr/${_target:+$CTARGET/}lib \

	mv "$pkgdir/$_gcclibexec/cc1plus" "$subpkgdir/$_gcclibexec/"

	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/*++* "$subpkgdir"/usr/${_target:+$CTARGET/}lib/
	mv "$pkgdir"/usr/${_target:+$CTARGET/}include/c++ "$subpkgdir"/usr/${_target:+$CTARGET/}include/
	mv "$pkgdir"/usr/bin/*++ "$subpkgdir"/usr/bin/
}

libobjc() {
	pkgdesc="GNU Objective-C runtime"
	replaces="objc"
	depends=
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libobjc.so.* "$subpkgdir"/usr/lib/
}

objc() {
	pkgdesc="GNU Objective-C"
	replaces="gcc"
	depends="libc-dev gcc=$_gccrel libobjc=$_gccrel"

	mkdir -p "$subpkgdir"/$_gcclibdir/include \
		"$subpkgdir"/usr/lib
	mv "$pkgdir"/$_gcclibdir/include/objc "$subpkgdir"/$_gcclibdir/include/
	mv "$pkgdir"/usr/lib/libobjc.so "$pkgdir"/usr/lib/libobjc.a \
		"$subpkgdir"/usr/lib/
}

libgcc() {
	pkgdesc="GNU C compiler runtime libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libgcc_s.so.* "$subpkgdir"/usr/lib/
}

libgomp() {
	pkgdesc="GCC shared-memory parallel programming API library"
	depends=
	replaces="gcc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libgomp.so.* "$subpkgdir"/usr/lib/
}

libgo() {
	pkgdesc="Go runtime library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgo.so.* "$subpkgdir"/usr/lib/
}

go() {
	pkgdesc="Go support for GCC"
	depends="gcc=$_gccrel libgo=$_gccrel"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/lib/go "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin/*gccgo "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/bin/*go "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/*gofmt "$subpkgdir"/usr/bin
	mv "$pkgdir"/$_gcclibexec/go1 "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/cgo "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/buildid "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/test2json "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/vet "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/usr/lib/libgo.a \
		"$pkgdir"/usr/lib/libgo.so \
		"$pkgdir"/usr/lib/libgobegin.a \
		"$pkgdir"/usr/lib/libgolibbegin.a \
		"$subpkgdir"/usr/lib/
}

libgfortran() {
	pkgdesc="Fortran runtime library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgfortran.so.* "$subpkgdir"/usr/lib/
}

libquadmath() {
	replaces="gcc"
	pkgdesc="128-bit math library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libquadmath.so.* "$subpkgdir"/usr/lib/
}

gfortran() {
	pkgdesc="GNU Fortran Compiler"
	depends="gcc=$_gccrel libgfortran=$_gccrel"
	$LIBQUADMATH && depends="$depends libquadmath=$_gccrel"
	replaces="gcc"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/$_gcclibdir \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/*gfortran "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/lib/libgfortran.a \
		"$pkgdir"/usr/lib/libgfortran.so \
		"$subpkgdir"/usr/lib/
	if $LIBQUADMATH; then
		mv "$pkgdir"/usr/lib/libquadmath.a \
			"$pkgdir"/usr/lib/libquadmath.so \
			"$subpkgdir"/usr/lib/
	fi
	mv "$pkgdir"/$_gcclibexec/f951 "$subpkgdir"/$_gcclibexec
	mv "$pkgdir"/usr/lib/libgfortran.spec "$subpkgdir"/$_gcclibdir
}

libgnat() {
	pkgdesc="GNU Ada runtime shared libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgna*.so "$subpkgdir"/usr/lib/
}

gnat() {
	pkgdesc="Ada support for GCC"
	depends="gcc=$_gccrel"
	[ "$CHOST" = "$CTARGET" ] && depends="$depends libgnat=$_gccrel"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/$_gcclibdir \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/$_gcclibexec/*gnat* "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibdir/*ada* "$subpkgdir"/$_gcclibdir/
	mv "$pkgdir"/usr/bin/*gnat* "$subpkgdir"/usr/bin/
}

sha512sums="92f599680e6b7fbce88bcdda810f468777d541e5fddfbb287f7977d51093de2a5178bd0e6a08dfe37090ea10a0508a43ccd00220041abbbec33f1179bfc174d8  gcc-8.5.0.tar.xz
6d84354e6df96d5ea244eb3bb5f044781796b88040b11c78fb6ee509e5aac19d46e0e92ca836e98e6495d9751f52439833b748efc419e4f5d5301fb549c4dcc9  0002-posix_memalign.patch
7f434a7350c9b06d0ae7cc18a569d813238483afa34b0801d112844a0dfe6164ae36b0416955fd4da7a8caa54672247f319a8ec7ce4b6a97a5f4e17ec083112a  0008-s390x-muslldso.patch
b8207c1be82b20fdad2596bb0a60469db52257456d0925bacf48b44899f4c6ece93c501347e1ffe92fd372c30f90d05a6d8d8078876abf4cf2deae3dbb1a1f58  0009-microblaze-pr65649.patch
66085c5555e6b91b6874d1782d5a1dc0ab1792889f9400f48cde9483f82b51b9e3a5de1efbba21a19fc5e664334f2188d0c2bc988d42335efa26118b3c85cc7f  0010-ldbl128-config.patch
cd3ba928121e8578ba9f73215e4d81ee3ebbab33e00b04cdfd62b46d21f9536297ae12dc021aad6e56f3b28d9d544727331bbe6db1e3438d7b9545b2c3250906  0011-m68k.patch
45d5bc11a89e5af77503ca06eec52d6d84e6fea0be021aaee1bcc39f8dd3c7b4baa4200cfa2b0688faa0f207d005db89c029eff32f362207aa3d2365b2f2363e  0012-static-pie.patch
dfedf48829cff22ee836ece743de20ad467b40e86c8f4034326a4c32df35b10affd69d6b9ab3382df2871a99ad0c2e99d4342183b59f9bca7f15112eed58ec8d  0016-invalid-tls-model.patch
625c02e03c2f1db04da12cc6a086ec85790a031a13df36486243fd9569cd17f7c8ebeec91ac16cc1f87c3ec1ffe4c421153e98a9aeb5eea35943a6f015d81f50  002_all_default-relro.patch
02b725b220e540077efef741e9d457f9e004fe53ae642a138e214875d076a60f7c2f27de0ed9a4225db2030fc9c3d2c5b0414c895b9eec0f5f48fad70e2fb029  005_all_default-as-needed.patch
9073a5ae46e46a44a77adba183db301194a3666afca07a8444a4c10dc2c087dd41d6065a051b1024454bb37bee06fabf4361e5fae62dab8d4c9620520924f606  006_as-needed-gold.patch
622fdbcbbf2feb86bd839af627ec3613c6d2c77b14d37d31165b19f73f45b3663a203efff5d224f194edb15eb62d3d5885e32f85d1b584f071e580fea4e12664  011_all_default-warn-format-security.patch
03cddde56b2a355f08716c8efcd5616606d42946c87831d77d55570636c46e351e81a04c7dabd3466d9406c40476bead857cfe1eceaf7f0d86dd2757c8cf4a9e  012_all_default-warn-trampolines.patch
d35a3ac7e13a4446921a90e1ff3eec1079840c845f9d523c868e24ae21f94cf69ba041de5341ebef96432a6f57598e223381d4286e8fb8baaa25906707f29fbd  020_all_msgfmt-libstdc++-link.patch
0a0bc72b9366158f5d23fff1928e756fdd212433bac6ab1f00d632f241382820db8db5d475ddf11ea020eaf7e2e71b12fb9b1c3c870cf84adf6c2b16f15aabca  051_all_libiberty-pic.patch
a3702c1f642412413d0ffc69f6439a3e101b2b56c16610f94c66556c88e52d01b0b6d1e57bfc5a972668f976c02fbf2fc22bcf707b000867e44744d9eb7b7566  201-ada.patch
152639228a189b211df660b5b7543e3744cf136e80fed7f1ac37a3bc1b692fba3431a0d62337e769ee8f323188acdf11092cb1b2ef5c75fafee9628d13bc4163  202-ibm-ldbl.patch
98473bcaa77903a223ca9b0d2087c0921b287a2816d308cc32c8fe009e6cbf5dd1ae7fba27794ab8d9c09e117fe534413d91a464d1218474fc123ce0adfdc2c1  205-nopie.patch
d08d7ead2de0429e5c9055d5b029ec2be9a8c821d22cecaf9b51f633652c493333f98963d9267fa2fa63850c50ae5eefd5f59e5910ec10d20044dac082182a8b  libgcc-always-build-gcceh.a.patch
fa59b0fb081d97f8f63506b8793699588a95c602b5d468140eb1e80456597e52e1cc45dc0b234ac8e60e2b0cd606d94d111c8b0ae64c0a2be1bc1b8a184ceb93  libgo-musl-1.2.3.patch
600fe5098dc54edaa9808fd5717af9dec058953f9ad37d49cfba1db4f7e9a7a8f02019342f75157fc575946fa693259422184de27b7ecc8386d9f3ecc0f7cc5d  gcc-4.9-musl-fortify.patch
dbe0ee917fc7668571722364ab7c806731e3a31e8bfa30b4941b28b16b877d2a32b4a3897ef533399a28f82d43cac9b28e92de0493f0e779046db56584e07fa4  gcc-6.1-musl-libssp.patch
fa62556719449caec6b2b434355bfbcaa5ae55ffe017b3e1f827f66a2aae21b79c571ee7a4ce723ea69169bc3a6447e73650991a200cc372adf2f102677518d7  gcc-pure64.patch
35d6d59f0b7b968f282f56767c9e0823a7bdc5aa0d450aca50fbd802649a7ca608b47671244a3faa208a9b0d6832cabb5a22724157dc817b2c0ad63d09f93282  fix-cxxflags-passing.patch
3f5bc334d9f73d06f5f7c876738d02356acdd08958bea0e4d2095ebf15c2c2ec4e411abdae0297505ae9a1699ca01b17338e853184e84663203b192b0d35fc19  ada-shared.patch
f4ef08454e28c8732db69115e4998ec153399e8d229dd27f923dbdcf57b68128a65640d026cc7f45b58ba8764ab1eb575d4eb6d6dfc550a87a183f8b94e76181  320-libffi-gnulinux.patch
8efd028febb962ae100be32f72f7450c4f77d67ad96a5f47072136cdf47043f9d54835a87c7ab5aaa0fa0108c4c7a97ba2d7732d5aaf2efce70fe1f7e1c39d6e  330-gccgo-link-to-ucontext.patch
24c8708f64b9b7945b3f07e9bbecf17ab8bcde2524f49cbd694ca2f6146ccc3f13bb027db282f326525ea79d50a2868fa3fa2b0d688a914b5ea77bbfd7e5cbe4  331-gccgo-use-real-off_t-type.patch
31f9d85faa8648d140834646fc2bc7733eb362bfb87d696c5f375f9c0a3e4c3df41708a9a37c49bc0757513a7704976b9a5e21aa5c2de4b380d47d7e2cbde214  332-gccgo-sysinfo.patch
ef6ff46d571814ce2f6db549bfb9b9f5cc55d46085b197c36627a72e4f355fcbeb41d04fabd3029f3bc536fc48155fe3feab58a7a6ece9dcd19409ea44c17b0b  333-gccgo-signal-ppc64.patch
ab7a19d2702d232c9c1242fbdf9bd782e195d94104c0278e3b9ce435060a169094525ca53b615b3a9e62786db77bba836ecd495d58b544df474b13348432c200  334-gccgo-signal-shell.patch
e755072a3c71ada6cde354dd258fae029919e1116048068368d38411ca28a6ad8856702498d6667b0dcbb1d09fcb3fd66669aa79739a5e574a9cd490bd92ce90  335-gccgo-signal-ppc32.patch
de0cc0f9356c9ee5ea7b0f954441b59115f4a8f8f63573d0c17b6537e6f37641cf137531b496fc192d38035c2c4ba8175d36cbd7da15cbfcf8dc18c049c0f111  336-gccgo-mmap64.patch
c2916948b028e1e990e1953875b884561c0f8dd105c1ec03073795df9a47ec2c627cbc95ca0ec98ab9177bf2b7c8458bf3fd09f780fa6c301995846f6317e366  337-gccgo-signal-sig34.patch
0bdf592ec8444c6e253f1a7626b4e618b2e18e6d0b5a4548f90903f9de80a7cf3ea536750c2743f436a05faa1e7b65e52a391da9732dce51c4b00e23853e5365  338-gccgo-syscall.patch
b32d496a3a04c2357200b75dbf7f667fb57bf5af4f0c68926bc58f6600e4f23caa48aad2eaf073f1408168cacf27e8f11a6062c65bf5bb67458eedef698871d4  339-gccgo-errstr.patch
4db4a00dff1d3cb3ecbc713659316782ac839478ff1fd7166cfa64a2c7cee0ba2a0d3fa23cb858af7d8f59a6ac2bdf40e63a45c307b89eefe2a4e888707f615b  340-gccgo-time64-stat.patch
1860593584f629d24d5b6db14b0a3412e9f93449b663aaa4981301a0923db0159314905e694f27366fbfef72dce06636ab6df86862b7e9e9564847e03bee82c1  add-classic_table-support.patch
67a75a94fdba69de96b98dbc2978a50cb197857c464b81f7c956176da7066b3be937e40cb15e0870fc1e7382d662c5101bcd18cf457fc4112de41802042b51c4  gcc-5.4.0-locale.patch
65a4d8bf9cefcbc79e86015ef4376b2794492d6cae77065359b35bb4ed630dde6256982cd5e43ed837cbbdab366ea376da9f1c83f80ddf6dc53ab017b378c3cd  backport-r267157-posix-conformant-snprintf.patch"
