# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
#
# secfixes:
#   2.4.2-r0:
#     - CVE-2017-0898
#     - CVE-2017-10784
#     - CVE-2017-14033
#     - CVE-2017-14064
#     - CVE-2017-0899
#     - CVE-2017-0900
#     - CVE-2017-0901
#     - CVE-2017-0902
#   2.4.3-r0:
#     - CVE-2017-17405
#   2.5.1-r0:
#     - CVE-2017-17742
#     - CVE-2018-6914
#     - CVE-2018-8777
#     - CVE-2018-8778
#     - CVE-2018-8779
#     - CVE-2018-8780
#   2.5.3-r0:
#     - CVE-2018-16395
#     - CVE-2018-16396
#   2.5.3-r2:
#     - CVE-2019-8320
#     - CVE-2019-8321
#     - CVE-2019-8322
#     - CVE-2019-8323
#     - CVE-2019-8324
#     - CVE-2019-8325
#   2.5.7-r0:
#     - CVE-2012-6708
#     - CVE-2015-9251
#     - CVE-2019-15845
#     - CVE-2019-16201
#     - CVE-2019-16254
#     - CVE-2019-16255
#   2.5.7-r1:
#     - CVE-2020-8130
#
pkgname=ruby
pkgver=3.1.2
_abiver="${pkgver%.*}.0"
pkgrel=0
pkgdesc="An object-oriented language for quick and easy programming"
url="https://www.ruby-lang.org/"
arch="all"
license="Ruby AND BSD-2-Clause"
depends="ca-certificates"
depends_dev="ruby=$pkgver-r$pkgrel gmp-dev libucontext-dev"
makedepends="$depends_dev zlib-dev openssl-dev db-dev libedit-dev
	libffi-dev coreutils yaml-dev linux-headers autoconf"
install="$pkgname.post-upgrade"
subpackages="$pkgname-doc $pkgname-dev
	$pkgname-bigdecimal
	$pkgname-fiddle
	$pkgname-io-console:io_console
	$pkgname-irb::noarch
	$pkgname-json
	$pkgname-minitest::noarch
	$pkgname-power_assert::noarch
	$pkgname-rake::noarch
	$pkgname-rdoc::noarch
	$pkgname-test-unit:test_unit:noarch
	$pkgname-libs
	$pkgname-full::noarch
	"
source="https://cache.ruby-lang.org/pub/ruby/${pkgver%.*}/$pkgname-$pkgver.tar.xz
	rubygems-avoid-platform-specific-gems.patch
	test_insns-lower-recursion-depth.patch
	fix-get_main_stack.patch
	libedit-compat.patch
	"
replaces="ruby-etc ruby-gems"

_gemdir="/usr/lib/ruby/gems/$_abiver"
_rubydir="/usr/lib/ruby/$_abiver"
_chost="$(printf '%s' "$CHOST" | sed 's/-foxkit-/-/')"

case "$CARCH" in
	x86 | pmmx) _arch="i386";;
	*) _arch="$CARCH";;
esac

_bundled_gems=""
for _i in $subpackages; do
	case "$_i" in
		*-dev | *-doc | *-libs | *-full:*) continue;;
		*) _bundled_gems="$_bundled_gems ${_i%%:*}";;
	esac
done

prepare() {
	default_prepare
	autoconf
}

build() {
	# -fomit-frame-pointer makes ruby segfault, see gentoo bug #150413
	# In many places aliasing rules are broken; play it safe
	# as it's risky with newer compilers to leave it as it is.
	export CFLAGS="$CFLAGS -fno-omit-frame-pointer -fno-strict-aliasing"
	export CPPFLAGS="$CPPFLAGS -fno-omit-frame-pointer -fno-strict-aliasing"

	# Needed for coroutine stuff
	export LIBS="-lucontext"

	# ruby saves path to install. we want use $PATH
	export INSTALL=install

	# the configure script does not detect isnan/isinf as macros
	export ac_cv_func_isnan=yes
	export ac_cv_func_isinf=yes

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--with-sitedir="/usr/local/lib/site_ruby" \
		--with-search-path="/usr/lib/site_ruby/\$(ruby_ver)/$_arch-linux" \
		--enable-pthread \
		--disable-rpath \
		--enable-shared \
		--with-mantype=man
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install

	install -m 644 -D COPYING \
		"$pkgdir"/usr/share/licenses/$pkgname/COPYING

	rm -R "$pkgdir"$_gemdir/cache/*

	if [ -d "$pkgdir"/usr/local ]; then
		f="$(cd "$pkgdir" ; find usr/local -type f)"
		if [ -n "$f" ]; then
			error "Found files in /usr/local:"
			echo "$f"
			return 1
		fi
		rm -r "$pkgdir"/usr/local
	fi
}

bigdecimal() {
	pkgdesc="Ruby arbitrary-precision floating point decimal arithmetic"
	license="Ruby"
	depends="$pkgname-libs"

	_mvgem bigdecimal
}

fiddle() {
	pkgdesc="A libffi wrapper for Ruby"
	license="BSD-2-Clause"
	depends=""

	_mvgem fiddle
}

io_console() {
	pkgdesc="Ruby simple console IO library"
	license="BSD-2-Clause"
	depends=""

	_mvgem io-console
	_mv $_rubydir/io \
		$_rubydir/$_chost/io/console.so
}

irb() {
	pkgdesc="The Interactive Ruby"
	replaces="$pkgname"
	depends="$pkgname ruby-io-console"

	cd "$pkgdir"
	_mv usr/bin/irb \
		usr/lib/ruby/$_abiver/irb*
}

json() {
	pkgdesc="JSON implementation as a Ruby extension in C"
	license="Ruby"
	depends=""

	_mvgem json
}

minitest() {
	pkgdesc="Ruby suite of testing facilities"
	license="MIT"
	depends="$pkgname"

	_mvgem minitest
}

power_assert() {
	pkgdesc="Power Assert for Ruby"
	depends="$pkgname-libs"

	_mvgem power_assert
}

rake() {
	pkgdesc="Ruby based make-like utility"
	license="MIT"
	depends="$pkgname"

	_mvgem rake
	_mv usr/bin/rake
}

rdoc() {
	pkgdesc="Ruby documentation tool"
	license="Ruby"
	depends="$pkgname $pkgname-json $pkgname-io-console"

	_mvgem rdoc
	_mv usr/bin/ri \
		usr/bin/rdoc
}

test_unit() {
	pkgdesc="An xUnit family unit testing framework for Ruby"
	license="Ruby PSFL"
	depends="$pkgname $pkgname-power_assert"

	_mvgem test-unit
}

libs() {
	pkgdesc="Libraries necessary to run Ruby"
	depends=""

	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr/
}

full() {
	pkgdesc="Ruby with all bundled gems"
	depends="ruby $_bundled_gems"

	mkdir -p "$subpkgdir"
}

_mv() {
	for i in "$@"; do
		mkdir -p "$subpkgdir"/${i%/*}
		mv "$pkgdir"/${i} "$subpkgdir"/${i%/*}/
	done
}

_mvgem() {
	cd "$pkgdir"
	for path in ./$_rubydir/${1}.rb \
			./$_rubydir/${1} \
			./$_rubydir/$_chost/${1} \
			./$_rubydir/$_chost/${1}.so \
			./$_gemdir/gems/${1}-* \
			./$_gemdir/specifications/${1}-* \
			./$_gemdir/specifications/default/${1}-* ; do
		[ ! -e "$pkgdir/$path" ] || _mv "$path"
	done
}

sha512sums="4a74e9efc6ea4b3eff4fec7534eb1fff4794d021531defc2e9937e53c6668db8ecdc0fff2bc23d5e6602d0df344a2caa85b31c5414309541e3d5313ec82b6e21  ruby-3.1.2.tar.xz
a142199140fa711a64717429e9069fd2082319abaf4b129f561db374b3bc16e2a90cc4c849b5d28334505d1c71fed242aef3c44d983da3513d239dcb778673a5  rubygems-avoid-platform-specific-gems.patch
814fe6359505b70d8ff680adf22f20a74b4dbd3fecc9a63a6c2456ee9824257815929917b6df5394ed069a6869511b8c6dce5b95b4acbbb7867c1f3a975a0150  test_insns-lower-recursion-depth.patch
3ffc034c01110ee5531265333ca5ee8d61d08131843fe3004c5b34c88c9c1b32cb4ed89574f393177c8bd526e9c15da61ab344f93adf07b9148c561ee19e2eb5  fix-get_main_stack.patch
6cbeb9d05a3c028e6bd371deaeec6e97fa8ebd03f0dc9d406648a03b569bb44a148e2bd81998a2a973e91ded8a08af141bdebe3e324bdc32df5e74209a42b024  libedit-compat.patch"
