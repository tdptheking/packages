# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=s6-linux-init
pkgver=1.0.8.0
pkgrel=0
pkgdesc="A s6-based init system"
url="https://skarnet.org/software/$pkgname/"
arch="all"
options="!check"  # No test suite.
license="ISC"
_skalibs_version=2.12
_s6_version=2.11.1.1
depends="execline s6>=$_s6_version s6-linux-init-common"
makedepends="skalibs-dev>=$_skalibs_version execline-dev s6-dev utmps-dev"
subpackages="$pkgname-common:common:noarch $pkgname-early-getty:earlygetty:noarch $pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz rc.init runlevel rc.shutdown reboot.sh earlygetty.run"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
provides="/sbin/init=0"

build() {
	./configure \
		--enable-shared \
		--enable-static \
		--enable-allstatic \
		--enable-static-libc \
		--libdir=/usr/lib \
		--libexecdir="/lib/$pkgname" \
		--with-dynlib=/lib \
		--enable-utmps
	make
}

package() {
	make DESTDIR="$pkgdir" install

	for i in rc.init runlevel rc.shutdown ; do
		cp -f "$srcdir/$i" "$pkgdir/etc/s6-linux-init/skel/"
		chmod 0755 "$pkgdir/etc/s6-linux-init/skel/$i"
	done

	# static binaries don't work under fakeroot, so build a dynamic one just for us
	touch src/init/s6-linux-init-maker.o
	make s6-linux-init-maker LDFLAGS_NOSHARED=""

	./s6-linux-init-maker \
	  -u catchlog \
	  -G "sleep 86400" \
	  -1 \
	  -L \
	  -p "/usr/bin:/usr/sbin:/bin:/sbin" \
	  -m 022 \
	  -s /run/kernel_env \
	  -f "$pkgdir/etc/s6-linux-init/skel" \
	  "$pkgdir/etc/s6-linux-init/current"
	mkdir -p -m 0755 "$pkgdir/sbin" "$pkgdir/etc/runlevels/empty"
	for i in init halt poweroff reboot shutdown telinit ; do
		ln -sf "../etc/s6-linux-init/current/bin/$i" "$pkgdir/sbin/$i"
	done
}


common() {
	pkgdesc="Files for an s6 supervision tree, common to s6-linux-init and sysvinit"
	depends="execline s6"
	runimg="$pkgdir/etc/s6-linux-init/current/run-image"
	subrunimg="$subpkgdir/etc/s6-linux-init/current/run-image"
	install="$subpkgname.post-upgrade $subpkgname.pre-deinstall"
	mkdir -p -m 0755 "$subrunimg/service/.s6-svscan" "$subrunimg/service/s6-svscan-log" "$subpkgdir/usr/share/s6-linux-init-common"
	mv "$runimg/uncaught-logs" "$subrunimg/"
	mv "$runimg/service/s6-svscan-log" "$subrunimg/service/"
	mkdir -m 0755 "$runimg/service/s6-svscan-log"
	mv "$subrunimg/service/s6-svscan-log/run" "$runimg/service/s6-svscan-log/"
	cp -f "$srcdir/reboot.sh" "$subpkgdir/usr/share/s6-linux-init-common/"
}


earlygetty() {
	pkgdesc="Files for a configurable early getty"
        depends="s6-linux-init-common"
        svcimg="$pkgdir/etc/s6-linux-init/current/run-image/service"
        subsvcimg="$subpkgdir/etc/s6-linux-init/current/run-image/service"
	mkdir -p -m 0755 "$subsvcimg"
	mv "$svcimg/s6-linux-init-early-getty" "$subsvcimg/"
	cp -f "$srcdir/earlygetty.run" "$subsvcimg/s6-linux-init-early-getty/run"
	chmod 0755 "$subsvcimg/s6-linux-init-early-getty/run"
}


libs() {
        pkgdesc="$pkgdesc (shared libraries)"
        depends="skalibs-libs>=$_skalibs_version"
        mkdir -p "$subpkgdir/lib"
        mv "$pkgdir"/lib/*.so.* "$subpkgdir/lib/"
}


dev() {
        pkgdesc="$pkgdesc (development files)"
        depends="skalibs-dev>=$_skalibs_version"
	install_if="dev $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr"
        mv "$pkgdir/usr/lib" "$pkgdir/usr/include" "$subpkgdir/usr/"
}


libsdev() {
        pkgdesc="$pkgdesc (development files for dynamic linking)"
        depends="$pkgname-dev"
        mkdir -p "$subpkgdir/lib"
        mv "$pkgdir"/lib/*.so "$subpkgdir/lib/"
}


doc() {
        pkgdesc="$pkgdesc (documentation)"
        depends=""
        install_if="docs $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr/share/doc"
        cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="b64319bc13a98e5f766a118ebd54ccec93163b38b2afc060fe4034a4f2312bb6ae68019e37706338f84c74aa527e5b2ac1f4b29d39255c2d436eab8608483346  s6-linux-init-1.0.8.0.tar.gz
756b0cbbe5dabb4631380c3c7ea199cc213224b2e36e50a2d012a61948170078b78bf49b85d886319fecf59843087f937d3d804723b2553ac9f94d088a2f0fd8  rc.init
e73c3c32b118831074288d23fadace2158a2b15d5a13ffa73290b92a9e39c2a21c73d3b0eabea29bcbaa5f6381611fd8d0aaa6aa691ec7de91b8ef6ae404b6da  runlevel
7bb050248a5c2ab6a56c50c35f87cde724f97ff9882f5e60b0f0f2f14bd93c1df7d99fedc3d81c8519cf1a1ed90e03f1cbb9bf891c7b3618aa9a5f5738d262f4  rc.shutdown
6fb2a1112988fd2322b4bc4862bfb948a1c2e43921c5d01ae873c0d31b39fe74fc2934a5018c08b1704a2b2199b31d3a3d7365be369bba734f153b74e000aa74  reboot.sh
dfff483b61370ce2c8fe653cb4e9b6ec7ef678f26b128eab8e677548a48b668b532b12a2e4618c85bf95777a666ac30189780708803d6ea12e43ab0072399212  earlygetty.run"
