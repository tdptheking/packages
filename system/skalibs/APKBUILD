# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=skalibs
pkgver=2.12.0.0
pkgrel=0
pkgdesc="A set of general-purpose C programming libraries for skarnet.org software"
url="https://skarnet.org/software/skalibs/"
arch="all"
options="!check"  # No test suite.
license="ISC"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--enable-shared \
		--enable-static \
		--libdir=/usr/lib
	make
}

package() {
	make DESTDIR="$pkgdir" install
}


libs() {
	pkgdesc="$pkgdesc (shared libraries)"
	depends=""
	mkdir -p "$subpkgdir/lib"
	mv "$pkgdir"/lib/*.so.* "$subpkgdir/lib/"
}


dev() {
	pkgdesc="$pkgdesc (development files)"
	depends=""
	install_if="dev $pkgname=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"
	mv "$pkgdir/usr" "$subpkgdir/"
}


libsdev() {
	pkgdesc="$pkgdesc (development files for dynamic linking)" 
	depends="$pkgname-dev"
	mkdir -p "$subpkgdir/lib"
	mv "$pkgdir"/lib/*.so "$subpkgdir/lib/"
}


doc() {
	pkgdesc="$pkgdesc (documentation)" 
	depends=""
	install_if="docs $pkgname=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir/usr/share/doc"
	cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="7a431b47bb5be1d6f647da65026c7d3fa9c836b6a5e9984ae8b46dda9263a738ad1f26d76b6fe12b750f8338e9506fc8472f467c6d7d5b8369cadd602d9131bf  skalibs-2.12.0.0.tar.gz"
