# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=frameworkintegration
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework providing components to allow applications to integrate with a KDE Workspace"
url="https://www.kde.org/"
arch="all"
options="!check"  # All tests require X11.
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 knotifications-dev
	kconfig-dev kconfigwidgets-dev kiconthemes-dev qt5-qtx11extras-dev
	knewstuff-dev kpackage-dev kxmlgui-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/frameworkintegration-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="4bf533b261c149a10ed4b4607f532b57b989f6e130c417a0398216ddbe1b4b3ab964009aa5c021bdc92960f8c25c7f5c66cf8606ed7956b25adfa401d0f08e68  frameworkintegration-5.74.0.tar.xz"
