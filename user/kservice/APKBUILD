# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kservice
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework for implementing plugins and services"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test suite won't build, broken tarball
license="LGPL-2.1-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kconfig-dev kcoreaddons-dev kcrash-dev
	kdbusaddons-dev ki18n-dev"
makedepends="$depends_dev cmake extra-cmake-modules kdoctools-dev python3
	flex bison doxygen qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kservice-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} \
		-Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="7756047069ff22d1e78494770ec3fb56a07ce96459a759b8275188f75a9e9e5d334df0f7bc7c09fdf6f357c0e144cc115ba077a15079f8598bb042f37e5ce490  kservice-5.74.0.tar.xz"
