# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ktouch
pkgver=20.08.1
pkgrel=0
pkgdesc="Touch typing tutor"
url="https://www.kde.org/applications/education/ktouch/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtxmlpatterns-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kdeclarative-dev kdoctools-dev ki18n-dev kitemviews-dev kcmutils-dev
	ktextwidgets-dev kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev
	libx11-dev libxcb-dev libxkbfile-dev qt5-qtquickcontrols2-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktouch-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ff47d10afbb44f7f790843e92b4cefc5fbbffeaba3cbd26d70a4a4d5d71de90870a4dbc4f4792161c953d0adc05054c59f6c4be6d36050db359a0c23a79c92f7  ktouch-20.08.1.tar.xz"
