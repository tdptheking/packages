# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Maintainer: Sheila Aman <sheila@vulpine.house> 
pkgname=libevent
pkgver=2.1.12
pkgrel=0
pkgdesc="An event notification library"
url="https://libevent.org/"
arch="all"
license="BSD-3-Clause"
depends=""
depends_dev="python3"
makedepends="$depends_dev openssl-dev"
subpackages="$pkgname-dev"
source="https://github.com/$pkgname/$pkgname/releases/download/release-$pkgver-stable/$pkgname-$pkgver-stable.tar.gz
	py3_rpcgen.patch
	"

# secfixes:
#   2.1.8-r0:
#   - CVE-2016-10195
#   - CVE-2016-10196
#   - CVE-2016-10197

builddir="$srcdir"/$pkgname-$pkgver-stable

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR=$pkgdir install
}

dev() {
	replaces="libevent"
	default_dev
	provides="$provides pc:libevent=$pkgver-r$pkgrel"
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="88d8944cd75cbe78bc4e56a6741ca67c017a3686d5349100f1c74f8a68ac0b6410ce64dff160be4a4ba0696ee29540dfed59aaf3c9a02f0c164b00307fcfe84f  libevent-2.1.12-stable.tar.gz
ca097528f88b0a86248be594b44eaa7edcb3a1ee2b808685c09aa7947bb5c10342f4b584e7f2fcef6bc4a185fecb80da4a7a6660dd5c075f3416f9a55a1389b0  py3_rpcgen.patch"
