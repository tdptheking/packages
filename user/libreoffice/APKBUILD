# Contributor: Jens Staal <staal1978@gmail.com>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Max Rees <maxcrees@me.com>

# You probably don't want "srcdir", "pkgdir", or "deps" in CLEANUP,
# and you definitely don't want "deps" in ERROR_CLEANUP.
# Just "abuild clean undeps" once you're completely done.
#
# Even with JOBS=4 you're gonna want at least 8 GB of RAM.
#
# * Disk space:
#   * Dependencies:  1557 MiB =  1.5 GiB
#   * Downloads:      515 MiB
#   * Subtotal:      2072 MiB =  2.0 GiB
#   * Complete src: 34134 MiB
#   * Complete pkg:  9399 MiB
#   * All APKs:      2282 MiB =  2.2 GiB
#   * Subtotal:     45815 MiB = 44.7 GiB
#   * Grand total:  47887 MiB = 46.8 GiB
#
# Build stats:
# * Run "abuild deps fetch" first.
# * time abuild -r
#
# * x86_64 Intel i7-4810MQ (4 core 2.8 GHz, turbo to 3.8 GHz, no HT)
#   JOBS=4, 8 GB RAM
#   abuild -r  29590.16s user 1077.69s system 340% cpu 2:30:13.12 total

pkgname=libreoffice
pkgver=6.4.0.3
pkgrel=0
case "$pkgver" in
*.*.*.*) _ver="${pkgver%.*}";;
*.*.*) _ver="$pkgver";;
esac
pkgdesc="LibreOffice - Meta package for the full office suite"
url="https://www.libreoffice.org/"
# While the metapackage technically is empty and should be "noarch", there's
# no easy way to change this to noarch and then change all of the subpackages
# to have the correct arch. Setting $arch in a split function is forbidden,
# and $subpackages doesn't really support setting $arch to anything other than
# noarch.
arch="all"
options=""
license="MPL-2.0 AND Apache-2.0 AND MIT AND X11 AND (MPL-1.1 OR GPL-2.0+ OR LGPL-2.1+) AND GPL-2.0+ AND GPL-3.0 AND GPL-3.0+ AND LGPL-2.1 AND LGPL-3.0+ AND BSD-3-Clause AND SISSL AND IJG AND CC-BY-SA-3.0"

depends="$pkgname-base=$pkgver-r$pkgrel
	$pkgname-calc=$pkgver-r$pkgrel
	$pkgname-common=$pkgver-r$pkgrel
	$pkgname-draw=$pkgver-r$pkgrel
	$pkgname-impress=$pkgver-r$pkgrel
	$pkgname-math=$pkgver-r$pkgrel
	$pkgname-connector-postgres=$pkgver-r$pkgrel
	$pkgname-writer=$pkgver-r$pkgrel
	"
# 1. Base dependencies
# 2. GUIs - gen
# 3. GUIs - gtk+2.0
# 4. File formats
makedepends="apr-dev autoconf automake bash bison boost-dev cairo-dev
	clucene-dev cmd:which coreutils cppunit-dev cups-dev dbus-glib-dev findutils
	flex fontconfig-dev fontforge-dev freetype-dev glm gperf gpgme-dev
	gst-plugins-base-dev gstreamer-dev harfbuzz-dev hunspell-dev hyphen-dev icu
	icu-dev lcms2-dev libcmis-dev libexttextcat-dev libjpeg-turbo-dev
	libnumbertext-dev libpng-dev libqrcodegen libxml2-utils libxslt-dev mdds~1.5
	mythes-dev neon-dev nss-dev openldap-dev openssl-dev perl poppler-dev
	postgresql-dev python3-dev py3-lxml redland-dev sane-dev sed ucpp
	unixodbc-dev util-linux xmlsec-dev zip

	libepoxy-dev libxinerama-dev libxrandr-dev libxrender-dev libxext-dev

	gtk+3.0-dev glib-dev mesa-dev

	libabw-dev libcdr-dev libe-book-dev libepubgen-dev libetonyek-dev
	libfreehand-dev libmspub-dev libmwaw-dev libodfgen-dev liborcus-dev~0.15
	libpagemaker-dev libqxp-dev libstaroffice-dev libvisio-dev libwpd-dev
	libwpg-dev libwps-dev libzmf-dev
	"

# -common also depends on these fonts
_fonts="ttf-liberation ttf-dejavu ttf-carlito"
checkdepends="$_fonts"

# The order here is important.
# -doc    comes first since it redirects manpages from the other subpackages
# -lang-* comes before -common since it redirects miscellaneous
#         language-specific files from -common
subpackages="$pkgname-doc $pkgname-base $pkgname-gtk3
	$pkgname-calc $pkgname-draw $pkgname-impress $pkgname-math
	$pkgname-connector-postgres $pkgname-writer"
source="https://download.documentfoundation.org/$pkgname/src/$_ver/$pkgname-$pkgver.tar.xz
	https://download.documentfoundation.org/$pkgname/src/$_ver/$pkgname-dictionaries-$pkgver.tar.xz
	https://download.documentfoundation.org/$pkgname/src/$_ver/$pkgname-translations-$pkgver.tar.xz
	https://download.documentfoundation.org/$pkgname/src/$_ver/$pkgname-help-$pkgver.tar.xz
	linux-musl.patch
	fix-execinfo.patch
	gettext-tiny.patch
	disable-crc-test.patch
	disable-outdated-font-test.patch
	disable-mariadbc.patch
	mso-lockfile-utf16le.patch
	poppler-0.83.patch
	"
ldpath="/usr/lib/$pkgname/program"

_languages="af:Afrikaans:MPL-2.0 AND Public-Domain AND LGPL-3.0 AND LGPL-2.1+ AND Apache-2.0
	am:Amharic:MPL-2.0 AND Apache-2.0
	an:Aragonese:MPL-1.1 OR GPL-3.0+ OR LGPL-3.0+
	ar:Arabic:MPL-2.0 AND (GPL-2.0+ OR LGPL-2.1+ OR MPL-1.1+) AND Apache-2.0
	as:Assamese:MPL-2.0 AND Apache-2.0
	ast:Asturian:MPL-2.0 AND Apache-2.0
	be:Belarusian:MPL-2.0 AND CC-BY-SA-3.0 AND Apache-2.0
	bg:Bulgarian:MPL-2.0 AND GPL-2.0+ AND Apache-2.0
	bn:Bengali:MPL-2.0 AND GPL-2.0 AND Apache-2.0
	bn_in:Bengali (India):MPL-2.0 AND Apache-2.0
	bo:Tibetan:MPL-2.0 AND Apache-2.0
	br:Breton:MPL-2.0 AND LGPL-3.0 AND Apache-2.0
	brx:Bodo:MPL-2.0 AND Apache-2.0
	bs:Bosnian:MPL-2.0 AND Apache-2.0
	ca:Catalan:MPL-2.0 AND GPL-2.0+ AND GPL-3.0+ AND (GPL-3.0+ OR LGPL-3.0+) AND Apache-2.0
	ca_valencia:Catalan (Valencian):MPL-2.0 AND Apache-2.0
	cs:Czech:MPL-2.0 AND Custom AND Apache-2.0
	cy:Welsh (Cymraeg):MPL-2.0 AND Apache-2.0
	da:Danish:MPL-2.0 AND (GPL-2.0 OR LGPL-2.1 OR MPL-1.1) AND Apache-2.0
	de:German:MPL-2.0 AND (GPL-2.0 OR GPL-3.0 OR OASIS-0.1) AND Apache-2.0
	dgo:Dogri proper:MPL-2.0 AND Apache-2.0
	dz:Dzongkha:MPL-2.0 AND Apache-2.0
	el:Greek:MPL-2.0 AND (GPL-2.0 OR LGPL-2.1 OR MPL-1.1) AND Apache-2.0
	en_gb:English (UK):MPL-2.0
	en_us:English (US):MPL-2.0 AND Custom AND MIT AND (MPL-1.1 OR GPL-3.0+ OR LGPL-3.0+) AND GPL-2.0+ AND LGPL-2.1+ AND Apache-2.0
	en_za:English (South Africa):MPL-2.0
	eo:Esperanto:MPL-2.0 AND Apache-2.0
	es:Spanish:MPL-2.0 AND (GPL-3.0 OR LGPL-3.0 OR MPL-1.1) AND LGPL-2.1 AND Apache-2.0
	et:Estonian:MPL-2.0 AND LGPL-2.1 AND LPPL-1.3c AND Apache-2.0
	eu:Basque:MPL-2.0 AND Apache-2.0
	fa:Persian (Farsi):MPL-2.0 AND Apache-2.0
	fi:Finnish:MPL-2.0 AND Apache-2.0
	fr:French:MPL-2.0 AND (MPL-1.1+ OR GPL-2.0+ OR LGPL-2.1+) AND LPPL-1.3c AND LGPL-2.1+ AND Apache-2.0
	ga:Irish:MPL-2.0 AND Apache-2.0
	gd:Scottish Gaelic:MPL-2.0 AND GPL-3.0 AND Apache-2.0
	gl:Galician:MPL-2.0 AND GPL-3.0 AND Apache-2.0
	gu:Gujarati:MPL-2.0 AND GPL-3.0 AND Apache-2.0
	gug:Guaraní (Paraguay):MPL-2.0 AND Apache-2.0
	he:Hebrew:MPL-2.0 AND GPL-3.0 AND Apache-2.0
	hi:Hindi:MPL-2.0 AND GPL-2.0+ AND Apache-2.0
	hr:Croatian:MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	hu:Hungarian:MPL-2.0 AND (GPL-2.0+ OR LGPL-2.1+ OR MPL-1.1+) AND Apache-2.0
	id:Indonesian:MPL-2.0 AND Apache-2.0
	is:Icelandic:MPL-2.0 AND CC-BY-SA-3.0 AND Apache-2.0
	it:Italian:MPL-2.0 AND GPL-3.0 AND LGPL-3.0 AND Apache-2.0
	ja:Japanese:MPL-2.0 AND Apache-2.0
	ka:Georgian:MPL-2.0 AND Apache-2.0
	kk:Kazakh:MPL-2.0 AND Apache-2.0
	km:Khmer:MPL-2.0 AND Apache-2.0
	kmr_latn:Kurmanji Kurdish (Latin):MPL-2.0 AND Apache-2.0
	kn:Kannada:MPL-2.0 AND Apache-2.0
	ko:Korean:MPL-2.0 AND Apache-2.0
	kok:Konkani:MPL-2.0 AND Apache-2.0
	ks:Kashmiri:MPL-2.0 AND Apache-2.0
	lb:Luxembourgish:MPL-2.0 AND Apache-2.0
	lo:Lao:MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	lt:Lithuanian:MPL-2.0 AND BSD-3-Clause AND LPPL-1.3c AND Apache-2.0
	lv:Latvian:MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	mai:Maithili:MPL-2.0 AND Apache-2.0
	mk:Macedonian:MPL-2.0 AND Apache-2.0
	ml:Malayalam:MPL-2.0 AND Apache-2.0
	mn:Mongolian:MPL-2.0 AND Apache-2.0
	mni:Meithei (Manipuri):MPL-2.0 AND Apache-2.0
	mr:Marathi:MPL-2.0 AND Apache-2.0
	my:Burmese:MPL-2.0 AND Apache-2.0
	nb:Norwegian (Bokmal):MPL-2.0 AND Apache-2.0
	ne:Nepali:MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	nl:Dutch:MPL-2.0 AND (BSD-2-Clause OR CC-BY-3.0) AND Apache-2.0
	no:Norwegian:GPL-2.0
	nn:Nynorsk:MPL-2.0 AND Apache-2.0
	nr:Ndebele (South):MPL-2.0 AND Apache-2.0
	nso:Northern Sotho:MPL-2.0 AND Apache-2.0
	oc:Occitan:MPL-2.0 AND GPL-2.0+ AND Apache-2.0
	om:Oromo:MPL-2.0 AND Apache-2.0
	or:Oriya:MPL-2.0 AND Apache-2.0
	pa_in:Punjabi (India):MPL-2.0 AND Apache-2.0
	pl:Polish:MPL-2.0 AND (GPL OR LGPL OR MPL OR CC-SA-1.0) AND LGPL-3.0 AND LGPL-2.1 AND Apache-2.0
	pt:Portuguese:MPL-2.0 AND (GPL-2.0 OR LGPL-2.1 OR MPL-1.1) AND GPL-2.0 AND Apache-2.0
	pt_br:Portuguese (Brazil):MPL-2.0 AND (LGPL-3.0 OR MPL-1.1) AND (GPL-3.0+ OR LGPL-3.0+ OR MPL-1.1) AND Apache-2.0
	ro:Romanian:MPL-2.0 AND (GPL-2.0 OR LGPL-2.1 OR MPL-1.1) AND GPL-2.0 AND GPL-2.0+ AND Apache-2.0
	ru:Russian:MPL-2.0 AND (MPL-1.1 OR GPL OR LGPL) AND Custom AND LGPL AND Apache-2.0
	rw:Kinyarwanda:MPL-2.0 AND Apache-2.0
	sa_in:Sanskrit (India):MPL-2.0 AND Apache-2.0
	sat:Santali:MPL-2.0 AND Apache-2.0
	sd:Sindhi:MPL-2.0 AND Apache-2.0
	si:Sinhala:MPL-2.0 AND GPL-3.0 AND Apache-2.0
	sid:Sidamo:MPL-2.0 AND Apache-2.0
	sk:Slovak:MPL-2.0 AND (GPL-2.0 OR LGPL-2.1 OR MPL-1.1) AND LPPL-1.3c AND MIT AND Apache-2.0
	sl:Slovenian:MPL-2.0 AND (GPL-2.0 OR LGPL-2.1) AND LGPL-2.1 AND Apache-2.0
	sq:Albanian:MPL-2.0 AND Apache-2.0
	ss:Swati:MPL-2.0 AND Apache-2.0
	st:Southern Sotho:MPL-2.0 AND Apache-2.0
	sv:Swedish:MPL-2.0 AND LGPL-3.0 AND Custom AND Apache-2.0
	sw_tz:Swahili (Tanzania):MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	ta:Tamil:MPL-2.0 AND Apache-2.0
	te:Telugu:MPL-2.0 AND GPL-2.0+ AND (GPL-3.0+ OR LGPL-3.0+) AND Apache-2.0
	tg:Tajik:MPL-2.0 AND Apache-2.0
	th:Thai:MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	tn:Tswana:MPL-2.0 AND Apache-2.0
	tr:Turkish:MPL-2.0 AND Apache-2.0
	ts:Tsonga:MPL-2.0 AND Apache-2.0
	tt:Tatar:MPL-2.0 AND Apache-2.0
	ug:Uyghur:MPL-2.0 AND Apache-2.0
	uk:Ukrainian:MPL-2.0 AND (GPL-2.0+ OR LGPL-2.1+ OR MPL-1.1) AND GPL-2.0+ AND Apache-2.0
	uz:Uzbek:MPL-2.0 AND Apache-2.0
	ve:Venda:MPL-2.0 AND Apache-2.0
	vi:Vietnamese:MPL-2.0 AND GPL-2.0 AND Apache-2.0
	xh:Xhosa:MPL-2.0 AND Apache-2.0
	zh_cn:Simplified Chinese (People's Republic of China):MPL-2.0 AND Apache-2.0
	zh_tw:Traditional Chinese (Taiwan):MPL-2.0 AND Apache-2.0
	zu:Zulu:MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	"
_lo_lang() {
	local lang="$1"
	case "$lang" in
	# e.g. zh_cn -> zh_CN
	*_[a-z][a-z]) lang="${lang%_*}_$(printf '%s' "${lang#*_}" | tr '[a-z]' '[A-Z]')";;
	# e.g. sr_latn -> sr_Latn
	*_latn) lang="${lang%_latn}_Latn";;
	esac
	printf '%s' "$lang"
}
_lo_languages=""
for _lang in $(printf '%s' "$_languages" | cut -d : -f 1); do
	subpackages="$subpackages $pkgname-lang-$_lang:_split_lang:noarch"
	# Seriously now. We even have secret languages that are not recognized
	# by the configure script. These two languages only have dictionaries.
	# c.f. _split_lang()
	[ "$_lang" = "an" ] || [ "$_lang" = "no" ] && continue

	# --with-languages seems to prefer dashes instead of underscores
	# when --with-myspell-dicts is given
	_lang="$(_lo_lang "$_lang" | tr _ -)"
	_lo_languages="$_lo_languages $_lang"
done
subpackages="$subpackages $pkgname-common"

# secfixes:
#   6.4.0.3-r0:
#     - CVE-2019-9848
#     - CVE-2019-9849
#     - CVE-2019-9850
#     - CVE-2019-9851
#     - CVE-2019-9852
#     - CVE-2019-9853
#     - CVE-2019-9854

prepare() {
	default_prepare
	NOCONFIGURE=1 ./autogen.sh
}

build() {
	export PYTHON="python3"
	# Note: --with-parallelism must be specified since getconf does not
	#       recognize _NPROCESSORS_ONLN

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-vendor="Adélie Linux" \
		--enable-symbols \
		--with-parallelism="$JOBS" \
		--disable-online-update \
		--disable-fetch-external \
		--disable-dependency-tracking \
		--enable-release-build \
		--enable-split-app-modules \
		--enable-python=system \
		--with-tls=nss \
		--with-system-libs \
		--with-system-ucpp \
		--with-help \
		--without-system-dicts \
		--with-external-tar="$srcdir" \
		--with-lang="$_lo_languages" \
		--with-myspell-dicts \
		--without-fonts \
		--disable-firebird-sdbc \
		--disable-coinmp \
		--disable-lpsolve \
		--enable-gtk3 \
		--disable-qt5 \
		--disable-odk \
		--disable-avahi \
		--disable-scripting-beanshell \
		--disable-scripting-javascript \
		--disable-sdremote \
		--disable-sdremote-bluetooth \
		--disable-pdfium \
		--disable-ooenv \
		--without-java \
		--disable-epm \
		--enable-build-opensymbol

	# adding '-isystem /usr/include' make things break with gcc6
	# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=823145
	sed -i -e 's:-isystem /usr/include[^/]::g' config_host.mk

	make build-nocheck
}

check() {
	make -k -j1 unitcheck
}

package() {
	make DESTDIR="$pkgdir" distro-pack-install
}

doc() {
	default_doc
	pkgdesc="LibreOffice - man pages"
	sed -i -e '\#^/usr/share/man#d' "$builddir"/file-lists/*.txt
}

_split() {
	local i
	for i in $(grep -v ^%dir "$builddir/file-lists/${1}_list.txt" | sort -u); do
		dirname="$(dirname $i)"
		[ -d "$subpkgdir/$dirname" ] || install -dm755 "$subpkgdir/$dirname"
		mv "$pkgdir/$i" "$subpkgdir/$i"
	done
}

_move_from() {
	orig_pkg="$1" # original owner of $path
	path="$2"     # file/directory to move
	dest="$3"     # destination directory, automatically under $subpkgdir

	sed -i -e "\\#^\\(%dir \\)*/${path#$pkgdir/}#d" \
		"$builddir/file-lists/${orig_pkg}_list.txt"
	[ -d "$subpkgdir/$dest" ] || install -dm755 "$subpkgdir/$dest"
	mv "$path" "$subpkgdir/$dest"
}

_split_lang() {
	local i lang entry dict dictdir auto autodir wiz wizdir logo logodir
	lang="${subpkgname#$pkgname-lang-}"
	entry="$(printf '%s' "$_languages" | grep "^\\s*$lang")"
	lang="$(_lo_lang "$lang")"

	pkgdesc="LibreOffice - $(printf '%s' "$entry" | cut -d : -f 2) language pack"
	license="$(printf '%s' "$entry" | cut -d : -f 3)"
	depends=""

	# Includes translations/messages and help packs initially
	_split "lang_$lang"
	# Everything else we must move by hand

	dictdir="usr/lib/libreoffice/share/extensions"
	case "$lang" in
	# en_US is installed by default, so it will own most of the English files
	en_US) dict="en";;
	pt) dict="pt-PT";;
	*) dict="$(printf '%s' "$lang" | tr _ -)";;
	esac
	if [ -d "$pkgdir/$dictdir/dict-$dict" ]; then
		_move_from common "$pkgdir/$dictdir/dict-$dict" "$dictdir"
	fi
	# Again, these languages only have dictionaries
	[ "$_lang" = "an" ] || [ "$_lang" = "no" ] && return 0

	autodir="usr/lib/libreoffice/share/autocorr"
	case "$lang" in
	de) auto="de";;
	en_US) auto="en-[A-Z][A-Z]";;
	en_*) auto="skip";;
	es) auto="es";;
	fr) auto="fr";;
	it) auto="it";;
	pt) auto="pt-PT";;
	pt_BR) auto="pt-BR";;
	*_[A-Z][A-Z]) auto="$(printf '%s' "$lang" | tr _ -)";;
	*_Latn) auto="${lang%_Latn}-Latn-[A-Z][A-Z]";;
	*) auto="$lang-[A-Z][A-Z]";;
	esac
	for i in $(find "$pkgdir/$autodir" -name "acor_$auto.dat"); do
		_move_from common "$i" "$autodir"
	done

	wizdir="usr/lib/libreoffice/share/wizards"
	case "$lang" in
	en_US) wiz="en_[A-Z][A-Z]";;
	en_*) wiz="skip";;
	*) wiz="$lang";;
	esac
	for i in $(find "$pkgdir/$wizdir" -name "resources_$wiz.properties"); do
		_move_from common "$i" "$wizdir"
	done

	logodir="usr/lib/libreoffice/share/Scripts/python/LibreLogo"
	case "$lang" in
	en_US) logo="en_[A-Z][A-Z]";;
	en_*) logo="skip";;
	*) logo="$lang";;
	esac
	for i in $(find "$pkgdir/$logodir" -name "LibreLogo_$logo.properties"); do
		_move_from common "$i" "$logodir"
	done
}

common() {
	pkgdesc="LibreOffice - common files"
	depends="$pkgname-lang-en_us=$pkgver-r$pkgrel $_fonts !$pkgname-gtk2"

	_split common

	mkdir -p "$subpkgdir/usr/share/appdata"
	mv "$pkgdir"/usr/share/appdata/*.xml "$subpkgdir/usr/share/appdata"

	mkdir -p "$subpkgdir"/usr/lib/libreoffice/share
	mv "$pkgdir"/usr/lib/libreoffice/share/libreofficekit \
		"$subpkgdir"/usr/lib/libreoffice/share/libreofficekit

	# At this point there should only be empty directories left in
	# the "libreoffice" metapackage
	if [ -n "$(find "$pkgdir" -type f)" ]; then
		error "Files still in 'libreoffice' package:"
		find "$pkgdir" -type f | sed "s#^$pkgdir/#\\t#"
		return 1
	fi
}

gtk3() {
	pkgdesc="LibreOffice - GTK+3.0 GUI"
	depends="libreoffice-common=$pkgver-r$pkgrel"
	install_if="$pkgname-common=$pkgver-r$pkgrel gtk+3.0"
	_split gnome
}

base() {
	pkgdesc="LibreOffice - database frontend"
	depends="libreoffice-common=$pkgver-r$pkgrel"
	_split base
}

calc() {
	pkgdesc="LibreOffice - spreadsheet editor"
	depends="libreoffice-common=$pkgver-r$pkgrel"
	_split calc
}

draw() {
	pkgdesc="LibreOffice - drawing application"
	depends="libreoffice-common=$pkgver-r$pkgrel"
	_split draw
}

impress() {
	pkgdesc="LibreOffice - presentation application"
	depends="libreoffice-common=$pkgver-r$pkgrel"
	_split impress
}

math() {
	pkgdesc="LibreOffice - equation editor"
	depends="libreoffice-common=$pkgver-r$pkgrel"
	_split math
}

postgres() {
	pkgdesc="LibreOffice - connector for PostgreSQL database"
	depends="libreoffice-base=$pkgver-r$pkgrel"
	_split postgresql
}

writer() {
	pkgdesc="LibreOffice - word processor"
	depends="libreoffice-common=$pkgver-r$pkgrel"
	_split writer
}

sha512sums="1ea4b5865b7ad7d7400a775e153b00aa01d188844b580b26adb0d513db5f29a880cac145a746f139d236fff8fb5e8ed316943551de2e2eea218ab96bf177ca42  libreoffice-6.4.0.3.tar.xz
6e6bd33c86b6fe0744b9db70982aca8032011ceda55db30a8214b822dd77ad4760518f58aa42fed10996081ce91bf791fc332a8273664d5dd2141cc88eaaba97  libreoffice-dictionaries-6.4.0.3.tar.xz
168800ec5526f69febf6f37df4d04c0c42d26595587339721141c40c9812d9398bef7618732244759d65b4ccf46072b65536504c37c95f73e1ab91e6582ee75d  libreoffice-translations-6.4.0.3.tar.xz
f3f0b5e9f09e55c49a74439e4355dfebc3fe0b6ec809eee44d58fa8493454828ea6af9aab6f336c74394aab0a71fa7d0aa3485d4f662f9917bfb798ad3164e13  libreoffice-help-6.4.0.3.tar.xz
91e3659232a6427234d483289892e1c66673ad2abef792cec9bf905f35797893faba59f10d775cdd0bad23d2be7b77578747dfa707d55bef62a2fbdc95ba3d2c  linux-musl.patch
ed18d6bc6e03369e17b50e6b969ac0804984c1416fdfe7120251d11c59a791bb0fc8956f848ee8697fce3c074f2943b03d4441aa531ff9504be2f497a8e7e412  fix-execinfo.patch
bac06a1f0f6ef3e5860ec340470583b2a7ff079efa9efee9119ae1ac320b97ecbfdb7eba63975d4f7a4e2476d3b01a9508a53a84b49df0a8febe12e17b685529  gettext-tiny.patch
ebfc1276a14354bb6b7274bd7850b7fe2b1653c379b1e04023c9fce65eaace4214b2e79d302a075b848297f454b3d340cf5e7db3565a69a2dfaecd4c98d6e669  disable-crc-test.patch
4ffc389a3c545d37d152bb52390c3abd06a32eb9fee03110c07e8b57f9b9d1be68fdc5092ad0be76f6540367a1f1d2e9e32d519021d8205847594edb811da428  disable-outdated-font-test.patch
a4b63e741cd0aa295440a79142f221f2526ae7ba4f2fd28f37366871970c539332de70d89dfe8682d917aa70131b7c3a703e592e88085b58d024659105f8066a  disable-mariadbc.patch
52f2e36b77fd952293d5bcd75d58aaee52faa846764d39c9f066778dfb7c7dcd3a6ab7ff6aeb8bd4a5671cec450888638f7c628c0302c7603e07b582c84ca556  mso-lockfile-utf16le.patch
38b09913075219e6d9bc34e94f995653941909118e96f89f7cf45798682758c541ff182b6643e45c7f20dfbb1c4c03e51ea4d8cdd15a997718f297c3c78245b5  poppler-0.83.patch"
