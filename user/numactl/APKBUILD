# Contributor: Daniel Sabogal <dsabogalcc@gmail.com>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=numactl
pkgver=2.0.14
pkgrel=0
pkgdesc="Simple NUMA policy support"
url="https://github.com/numactl/numactl"
# ARM lacks the __NR_migrate_pages syscall
arch="all !armhf !armv7"
license="GPL-2.0+ AND LGPL-2.1"
depends=""
makedepends="autoconf automake libtool linux-headers"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools"
source="$pkgname-$pkgver.tar.gz::https://github.com/numactl/$pkgname/archive/v$pkgver.tar.gz
	musl.patch
	"

prepare() {
	default_prepare
	./autogen.sh
}

build() {
	./configure \
		--prefix=/usr \
		--mandir=/usr/share/man
	make
}

check() {
	make check VERBOSE=1 TESTS='test/distance test/nodemap test/tbitmap'
}

package() {
	make DESTDIR="$pkgdir" install

	# provided by linux man-pages
	rm -r "$pkgdir"/usr/share/man/man2
}

tools() {
	pkgdesc="NUMA policy control tools"

	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr
}

sha512sums="adaf405f092fd9653f26d00f8c80cb83852c56ebd5d00e714e20d505008e74aa7105b0fb7aa55a605deac0d1491ceff57de931037d33e7944fca105bc6510ed4  numactl-2.0.14.tar.gz
c24affa5a8a8ea83d7f0ee384dc0629e17a5c4201357132f770f894ad4236772116d96d8389d54fb99095af40d1ccbffc3170b5fb9cc88cfca39179f50bee9c9  musl.patch"
