# Contributor: Sheila Aman <sheila@vulpine.house>
# Maintainer: Sheila Aman <sheila@vulpine.house>
pkgname=uchardet
pkgver=0.0.7
pkgrel=0
pkgdesc="Universal character encoding detection library"
url="https://www.freedesktop.org/wiki/Software/uchardet/"
arch="all"
license="MPL-1.1 OR GPL-2.0+ OR LGPL-2.1+"
depends=""
makedepends="cmake"
subpackages="$pkgname-dev $pkgname-doc"
source="https://www.freedesktop.org/software/uchardet/releases/uchardet-$pkgver.tar.xz
	no-debug-suffix.patch"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ddb7b63dd09c1d9acbe620d86217e583d9aa5340780ab4010ec9faa4fd331498859d5efa7829bf8847da89325accf8f7304b51d410210178fc1ffa6658064a6f  uchardet-0.0.7.tar.xz
2e6f9d34daa6ba7dab07843368017448a17162d4d55bbc01665d00afa61a36f8b44fd6149706b806c8980632bdf33d073e0a93559d809852e18460551f330ecb  no-debug-suffix.patch"
